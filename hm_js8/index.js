// 1.DOM об'єктна модель документа: представлення веб-сторінки у вигляді дерева елементів.
// 2. На відміну від innerHTML, innerText відобразить наявні теги у вигляді тексту. innerHTML дозволяє створити елемент HTML.
// 3. Елемент можна знайти за тегом, класом, атрибутом. Найчастіше для цього використовується метод querySelector() для одного елемента або querySelectorAll() для кількох.

const paragraphCollection = document.querySelectorAll("p");

console.log(paragraphCollection);

paragraphCollection.forEach((p) => p.style = "background-color: #ff0000");

const idElement = document.getElementById('optionsList');

console.log(idElement);
console.log(idElement.parentElement);
console.log(idElement.childNodes);

const testParagraph = document.querySelector('#testParagraph')

testParagraph.innerText = "This is a paragraph";

const mainHeaderClass = document.querySelector('.main-header');

const headerChildren = mainHeaderClass.children;

console.log(headerChildren);

for (const child of headerChildren) {
    child.classList.add('nav-item');
}

const sectionTitles = document.querySelectorAll('.section-title');

sectionTitles.forEach((subtitle) => subtitle.classList.remove('section-title'))
