
import styles from './App.module.scss';
import "./style.scss";
import React from 'react';
import Button from './components/Buttons/Button'
import Modal from './components/Modals/Modal'

  
class App extends React.PureComponent {

  state ={
    
    isOpenModalDelete:false,
    isOpenModalEdit:false,
    
  } 

  closeModal = ()=>{
    this.setState({isOpenModalDelete:false,
      isOpenModalEdit:false,})
  }

  openModalDelete = ()=>{
    this.setState({isOpenModalDelete:true,
      isOpenModalEdit:false
  })
}

openModalEdit = ()=>{
  this.setState({isOpenModalEdit:true,
    isOpenModalDelete:false
})
}

 render() {
 const { isOpenModalDelete, isOpenModalEdit} = this.state
  return  <div className={styles.container}>
  <div className={styles.buttonWrapper}>      
      <Button backgroundColor = "#2f4f4f" handleClick={()=>{this.openModalDelete()}} text = "Open first modal"/> 
      <Button backgroundColor = "lightslategrey" handleClick={()=>{this.openModalEdit()}} text = "Open second modal"/>
  </div>
  
 {isOpenModalDelete && 
 
      <Modal onClick={() =>this.closeModal()} actions ={<div className={styles.buttonModalWrapper}>      
      <Button backgroundColor = "#d44637" type="button" className={styles.buttonModal} handleClick={() =>{this.closeModal()}} text = "Ok"/>
      <Button backgroundColor = "#d44637" type="button" className={styles.buttonModal} handleClick={() =>{this.closeModal()}}text = "Cancel"/>
      </div>} header = {"Do you want to delete this file?"} text = {"Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"} closeButton />      
      
}

{isOpenModalEdit &&  
      <Modal onClick={() =>this.closeModal()} actions ={<div className={styles.buttonModalWrapper}>      
      <Button backgroundColor = "#d44637" type="button" className={styles.buttonModal} handleClick={() =>{this.closeModal()}} text = "Edit"/>
      <Button backgroundColor = "#d44637" type="button" className={styles.buttonModal} handleClick={() =>{this.closeModal()}} text = "Post it"/>
      <Button backgroundColor = "#d44637" type="button" className={styles.buttonModal} handleClick={()=>{this.closeModal()}}text = "Exit"/>
      </div>} header = {"Do you want to edit this file?"} text = {"You may edit this file, if you want. Press button for edit"} />
        
      }
</div>


}
}
export default App;
