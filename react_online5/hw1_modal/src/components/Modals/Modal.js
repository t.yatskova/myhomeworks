import React from "react";
import styles from "./Modal.module.scss"

class Modal extends React.Component {
    render() {
    const { header, closeButton, text, actions, onClick} = this.props; 
    return(
      <>
      <div className={styles.modalContainer} onClick = {onClick}></div>
        <div className={styles.modal}>
      <div className = {styles.modalHeader}>        
        <h3 className={styles.modalTitle}>{header}</h3>
        {closeButton &&  <div className={styles.modalClose} onClick = {onClick}>&times;</div>}
       
      </div>
      <p  className={styles.modalText}>{text}</p>
      {actions}   


</div>
</>
    )}
}

export default Modal