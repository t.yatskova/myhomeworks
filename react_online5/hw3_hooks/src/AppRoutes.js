import { Routes, Route } from 'react-router-dom'
import Main from './components/Pages/Main/Main'
import FavouriteList from './components/Pages/FavouriteList/FavouriteList'
import CartList from './components/Pages/CartList/CartList'

const AppRoutes = (props) => {
    const { cards, selected, addToCart, selectedToggle, removeFromCart, cart, toggleModal, } = props
    return (
        <Routes>
            <Route path='/' element={<Main
                cards={cards}
                cart={cart}
                selected={selected}
                addToCart={addToCart}               
                selectedToggle={selectedToggle}
                toggleModal={toggleModal}
              />} />

            <Route path='/favourite' element={<FavouriteList
                selected={selected}
                selectedToggle={selectedToggle}                
                toggleModal={toggleModal}
                addToCart={addToCart} />} />

            <Route path='/cart' element={<CartList
                cart={cart}
                selectedToggle={selectedToggle} 
                removeFromCart={removeFromCart}  
                toggleModal={toggleModal}
               
                selected={selected}
                 />} />



        </Routes>
    )
}

export default AppRoutes