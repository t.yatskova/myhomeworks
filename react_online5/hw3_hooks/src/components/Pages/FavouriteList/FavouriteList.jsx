
import CardsWrapper from '../../CardsWrapper/CardsWapper'
import { Link } from "react-router-dom";
import styles from './FavouriteList.module.scss'
import PropTypes, { number } from 'prop-types'
import { useEffect, useState } from "react";

const FavouriteList = (props) => {
    const { selected, selectedToggle, toggleModal,  addToCart } = props

    const [isListEmpty, setIsListEmpty] = useState(true);

    useEffect(() => {
        if (selected.length === 0) {
          setIsListEmpty(true)
        } else {
          setIsListEmpty(false)
        }
      }, [selected.length])

    return (
        <>
            <h1>Favourite</h1>
            {isListEmpty ? (
                <div>No selected items yet</div>
            ):(
                <CardsWrapper
                list={selected}
                selectedToggle={selectedToggle}
                textButton="Add to Card"
                handlerClick={addToCart}
                toggleModal={toggleModal}
                />
            )}
            
       
        </>
    )
}

export default FavouriteList 