
import CardsWrapper from '../../CardsWrapper/CardsWapper'
import styles from './Main.module.scss'
import PropTypes from 'prop-types'

const Main =(props)=> {
    
        const { cards, selected, cart, addToCart, toggleModal,  selectedToggle} = props
        return (
            <CardsWrapper
            list={cards}
            selected={selected}
            handlerClick={addToCart}
            toggleModal={toggleModal}
            textButton="Add to Card"
            selectedToggle={selectedToggle}
            cart={cart}
            />
          
        )
    
}

Main.propTypes = {
    cards: PropTypes.array.isRequired,
    selected: PropTypes.array.isRequired,  
    addToCart: PropTypes.func,
    toggleModal: PropTypes.func,
    selectedToggle:PropTypes.func,
    
    
}
Main.defaultProps = {
    toggleModal: () =>{},
    addToCart: () =>{},
    selectedToggle:() =>{}
}

export default Main