import CardsWrapper from '../../CardsWrapper/CardsWapper'
import { Link } from "react-router-dom";
import styles from './CartList.module.scss'
import { useEffect, useState } from "react";
import PropTypes, { number } from 'prop-types'

const CartList = (props)=>{
    const { cart, selected, removeFromCart, selectedToggle, incrementCartItem, dicrementCartItem, toggleModal} = props

    const [isListEmpty, setIsListEmpty] = useState(true);
    useEffect(() => {
      if (cart.length === 0) {
        setIsListEmpty(true);
      } else {
        setIsListEmpty(false);
      }
    }, [cart.length]);
    return(
        <>
        <h1>Cart</h1>
        {isListEmpty ? 
        (<div>"Not yet product items"</div>):
             
        (
            <CardsWrapper 
              list={cart}
              selectedToggle={selectedToggle}
              toggleModal={toggleModal}             
              selected={selected}
              handlerClick={removeFromCart}
              textButton="Delete from cart"
            />
          )}
       
           
   
    </>
    )
}

export default CartList