import { NavLink } from "react-router-dom"
import styles from './Navigation.module.scss'

const Navigation = ()=>{
    return( 
    <nav>
     <ul className={styles.navList}>
         <li className={styles.navItem}>
             <NavLink className={({ isActive })=> isActive ? styles.activeLink : styles.navLink} to="/">home</NavLink>
         </li>
         <li className={styles.navItem}>
             <NavLink className={({ isActive })=> isActive ? styles.activeLink :styles.navLink} to="/favourite">favourite</NavLink>
         </li>
         <li className={styles.navItem}>
             <NavLink className={({ isActive })=> isActive ? styles.activeLink :styles.navLink} to="/cart">cart</NavLink>
         </li>
     </ul>
    
 </nav>
    )
 }
 
 export default Navigation