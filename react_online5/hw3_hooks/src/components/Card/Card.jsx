import  book from '../../svg/book-4-svgrepo-com.svg'
import book2 from '../../svg/book-red.svg'
// import ButtonWrapper from '../ButtonWrapper/ButtonWapper';
import Button from '../Button/Button';
import styles from './Card.module.scss'
import PropTypes from 'prop-types'

const Card = (props)=>{
 
        const {card, selected, selectedToggle, cart, textButton, handlerClick, toggleModal} = props

        return (
            <li className={styles.card}>
                <h3> {card.title}</h3>
               <button className={styles.cardButtonLiked} onClick={()=>{selectedToggle(card.id)}}><img width ="15px"  height="15px" src={card.isSelected ? book2 : book} alt="favorite" title ="add to favorites"></img></button>
                {/* <div className={styles.cardWrapper}> */}
                <img className={styles.cardImg} src={card.img} alt={card.title}></img>
                {/* </div> */}
                <p> Article: {card.id}</p>
                <p> Cover: {card.color}</p>
                <span> Price:{card.price}</span>
                <Button textButton = {textButton}  onClick={()=>{ toggleModal(true)}} backgroundColor={'darkgray'}/>
                  
               
            </li>
        )
    
}

Card.propTypes = {
    card: PropTypes.shape({
    img: PropTypes.string,
    title: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number,]).isRequired,
    isSelected: PropTypes.bool, 
    id:  PropTypes.number}),
    
    incrementLiked: PropTypes.func,
           
    likedHandler: PropTypes.func,
}
Card.defaultProps = {
    img: "",
    color: "",
    incrementLiked: ()=>{},
    isSelected: false,    
    likedHandler: ()=>{},
}


export default Card