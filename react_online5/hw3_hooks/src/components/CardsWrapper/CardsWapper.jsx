
import Card from '../Card/Card'
import styles from './CardsWrapper.module.scss'
import PropTypes from 'prop-types'

const CardsWrapper =(props)=> {
    
        const { list, selected, cart, handlerClick, toggleModal, textButton, selectedToggle} = props
        return (
            <ul className={styles.cardsWrapper}>
                {list.map((card)=>(
                     <Card
                     key = {card.id}
                     card={card}
                     handlerClick = {handlerClick}
                     selectedToggle={selectedToggle}
                     selected={selected}
                     cart={cart}
                     toggleModal = {toggleModal}
                     textButton={textButton}
                    
                     />
                ))}
               
            </ul>
        )
    
}

CardsWrapper.propTypes = {
    list: PropTypes.array.isRequired,
    // selected: PropTypes.array.isRequired,  
    addToCart: PropTypes.func,
    toggleModal: PropTypes.func,
    selectedToggle:PropTypes.func,
    textButton: PropTypes.string
    
}
CardsWrapper.defaultProps = {
    toggleModal: () =>{},
    addToCart: () =>{},
    selectedToggle:() =>{}
}

export default CardsWrapper