// import React, {PureComponent} from "react";
import ButtonWrapper from '../ButtonWrapper/ButtonWrapper';

import styles from "./Modal.module.scss"

const Modal = (props)=> {
   
      
    const {header, text, closeButton, incrementCart, textButton, toggleModal, backgroundColor} = props; 
    return(
      <>
      <div className={styles.modalContainer} onClick={toggleModal}></div>
        <div className={styles.modal}>
      <div className = {styles.modalHeader}>        
        <h3 className={styles.modalTitle}>{header}</h3>
        {closeButton &&  <div className={styles.modalClose} onClick={toggleModal}>&times;</div>}
       
      </div>
      <p  className={styles.modalText}>{text}</p>
      <ButtonWrapper textButton = {textButton} incrementCart={incrementCart}  onClick={toggleModal} backgroundColor={backgroundColor} /> 


</div>
</>
    )
}

export default Modal