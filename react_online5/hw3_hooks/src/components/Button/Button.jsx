// import React from "react";
import styles from "./Button.module.scss"
import PropTypes from 'prop-types'


const Button = (props)=>{
    const {textButton, onClick, backgroundColor} = props;
    return(
        <button type="button" style ={{ backgroundColor}}className={styles.btn} onClick={onClick}>{textButton}</button>
    )
}

Button.propTypes = {
    textButton: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string.isRequired,
  }

export default Button


// class Button extends React.PureComponent {
   
//     render(){
//         const {textButton, toggleModal, backgroundColor} = this.props;
//         return(
//         <button type="button" style ={{ backgroundColor}}className={styles.btn} onClick={()=>toggleModal}>{textButton}</button>
      
//   )
// }
// }
// Button.propTypes = {
//   textButton: PropTypes.string.isRequired,
//   toggleModal: PropTypes.func.isRequired,
//   backgroundColor: PropTypes.string.isRequired,
// }

// export default Button;




