// import {  } from "react";

import styles from "./ButtonWrapper.module.scss";
import Button from "../Button/Button"
// import PropTypes from 'prop-types'

const ButtonWrapper= ({ textButton, onClick, backgroundColor})=>{
 
        return (
            <div className={styles.buttonWrapper}>
                <Button textButton={textButton} onClick={onClick} backgroundColor={backgroundColor}/>
            </div>
        )
 
}

export default ButtonWrapper