
import {useState, useEffect} from 'react'
import Header from './components/Header/Header'
// import Main from './components/Main/Main'
import Modal from './components/Modal/Modal'
import AppRoutes from './AppRoutes'
import styles from './App.module.scss';

const App = () =>{

  // useState
  const [cards, setCards] = useState([]) 
  const [selectedCounter, setSelectedCounter] = useState(0)
  const [selected, setSelected] = useState(JSON.parse(localStorage.getItem('selected')) ||[])

  const [cartCounter, setCartCounter] = useState(0)  
  const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) ||[])

  const [modalProps, setModalProps] = useState({})
  const [isOpenModal, setIsOpenModal] = useState(false)

// useEffect

 useEffect(() => {
  fetch('./products.json').then(res => res.json()).then(result => {

    setCards(result)})
  }, []);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  useEffect(() => {
    localStorage.setItem("selected", JSON.stringify(selected));
  }, [selected]);

// cart

const addToCart = (card) => {
  const isBasketIdList = cart.map((el) => el.id);
  const isInBasket = isBasketIdList.includes(card.id);
  if (!isInBasket) {
    setCart([...cart, card]);
    // closeModal();
  }
};

const removeFromCart = (card) => {
  // const filter = cart.filter((el) => el.id !== card.id);
  setCart(cart.filter((el) => el.id !== card.id));
  // closeModal();
};


// select
const selectedToggle = (id) => {
  const selectedCard = cards.find((card) => card.id === id);
  selectedCard.isSelected = true;
  const isSelectedCard = selected.find((card) => card.id === id);
  if (isSelectedCard) {   
    setSelected(selected.filter((item) => item.id !== id));
    selectedCard.isSelected = false;
  } else {
    setSelected([...selected, selectedCard]);
  }
};


  const toggleModal= (value) => {
    setIsOpenModal(value)
}


 
    return (
      <div className={styles.App}>
        <Header selectedCounter={selected.length} cartCounter={cart.length}/>
        <AppRoutes 
        cards={cards} 
        cart={cart}
        addToCart={addToCart}
        removeFromCart={removeFromCart}       
        selected={selected}
        selectedToggle={selectedToggle}
        toggleModal={toggleModal}    
        
       />
       
        
        {isOpenModal &&  <Modal isOpenModal={isOpenModal} closeButton  toggleModal={()=>{toggleModal(false)}}/>}
       
      </div>
    );
  

}

export default App;
