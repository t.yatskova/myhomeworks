import React from "react";
import styles from "./Button.module.scss"
import PropTypes from 'prop-types'

class Button extends React.PureComponent {
   
    render(){
        const {textButton, toggleModal, backgroundColor} = this.props;
        return(
        <button type="button" style ={{ backgroundColor}}className={styles.btn} onClick={()=>toggleModal(true)}>{textButton}</button>
      
  )
}
}
Button.propTypes = {
  textButton: PropTypes.string.isRequired,
  toggleModal: PropTypes.func.isRequired,
  backgroundColor: PropTypes.string.isRequired,
}

export default Button;