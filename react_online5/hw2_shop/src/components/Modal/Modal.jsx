import React, {PureComponent} from "react";
import ButtonWrapper from '../ButtonWrapper/ButtonWapper';

import styles from "./Modal.module.scss"

class Modal extends PureComponent {
    render() {
      
    const {header, text, closeButton, incrementCart, textButton, toggleModal, backgroundColor} = this.props; 
    return(
      <>
      <div className={styles.modalContainer} onClick={()=>toggleModal(false)}></div>
        <div className={styles.modal}>
      <div className = {styles.modalHeader}>        
        <h3 className={styles.modalTitle}>{header}</h3>
        {closeButton &&  <div className={styles.modalClose} onClick={()=>toggleModal(false)}>&times;</div>}
       
      </div>
      <p  className={styles.modalText}>{text}</p>
      <ButtonWrapper textButton = {textButton} incrementCart={incrementCart}  toggleModal={toggleModal} backgroundColor={backgroundColor} /> 


</div>
</>
    )}
}

export default Modal