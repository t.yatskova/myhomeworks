import React, { PureComponent } from 'react';
import styles from './Header.module.scss'
import  cartIcon from '../../svg/basket-svgrepo-com.svg'
import  isFavorite from '../../svg/book-4-svgrepo-com.svg'
import PropTypes from 'prop-types'

class Header extends PureComponent {
    render() {
        const {likedCounter,  cartCounter} = this.props
        return (
            <header className={styles.header}>
                <div className={styles.headerContent}>
                <h1 className={styles.headerTitle}>Around books</h1>
                <ul className={styles.headerNav}>
                    <li className={styles.headerNavItem}>
                        <button className={styles.headerNavIcon}><img src={isFavorite} alt="favorite"></img></button>
                        <span className={styles.headerNavCounter}>{likedCounter}</span>
                    </li>
                    <li className={styles.headerNavItem}>
                    
                    <button className={styles.headerNavIcon}><img src={cartIcon} alt="chart"></img></button>
                     <span className={styles.headerNavCounter}>{cartCounter}</span>
                    </li>


                </ul>
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    likedCounter: PropTypes.number,
    cartCounter: PropTypes.number,
}
Header.defaultProps = {
    likedCounter: 0,
    cartCounter: 0,
}


export default Header