import React, { PureComponent } from "react";
import styles from "./ButtonWrapper.module.scss";
import Button from "../Button/Button"
class ButtonWrapper extends PureComponent {
    render() {
        const { textButton, toggleModal, backgroundColor} = this.props;
        return (
            <div className={styles.buttonWrapper}>
                <Button textButton={textButton} toggleModal={toggleModal} backgroundColor={backgroundColor}/>
            </div>
        )
    }
}

export default ButtonWrapper