import {PureComponent} from 'react'
import Card from '../Card/Card'
import styles from './ListCards.module.scss'
import PropTypes from 'prop-types'

class ListCards extends PureComponent {
    render() {
        const { cards, likedHandler, incrementLiked, toggleModal, textButton} = this.props
        return (
            <ul className={styles.cardsWrapper}>
                {cards.map(({id, color, title, isSelected, img, price})=>(
                     <Card
                     key = {id}
                     id = {id}
                     color = {color}
                     title = {title}
                     isSelected = {isSelected}
                     img = {img}
                     price = {price}
                     likedHandler = {likedHandler}
                     incrementLiked = {incrementLiked}
                     toggleModal = {toggleModal}
                     textButton={textButton}
                    
                     />
                ))}
               
            </ul>
        )
    }
}

ListCards.propTypes = {
    cards: PropTypes.array.isRequired,
    incrementLiked: PropTypes.func,
    toggleModal: PropTypes.func.isRequired,    
    likedHandler: PropTypes.func,
}
ListCards.defaultProps = {
    incrementLiked: () =>{},    
    likedHandler: () =>{},
}

export default ListCards