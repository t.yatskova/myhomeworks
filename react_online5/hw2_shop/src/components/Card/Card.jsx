import React, { PureComponent} from 'react';

import  book from '../../svg/book-4-svgrepo-com.svg'
import book2 from '../../svg/book-red.svg'
import ButtonWrapper from '../ButtonWrapper/ButtonWapper';
// import Button from '../Button/Button';
import styles from './Card.module.scss'
import PropTypes from 'prop-types'

class Card extends PureComponent {
    render() {
        const {id, color, title, isSelected, img, price, likedHandler, incrementLiked, textButton, backgroundColor, toggleModal} = this.props
        return (
            <li className={styles.card}>
                <h3> {title}</h3>
               <button className={styles.cardButtonLiked} onClick={()=>{incrementLiked(isSelected); likedHandler(id, isSelected)}}><img width ="15px"  height="15px" src={isSelected ? book : book2} alt="favorite" title ="add to favorites"></img></button>
                {/* <div className={styles.cardWrapper}> */}
                <img className={styles.cardImg} src={img} alt={title}></img>
                {/* </div> */}
                <p> Article: {id}</p>
                <p> Cover: {color}</p>
                <span> Price:{price}</span>
                <ButtonWrapper textButton = {textButton}  onClick={()=>toggleModal(true)} backgroundColor={backgroundColor}/>
                  
               
            </li>
        )
    }
}

Card.propTypes = {
    img: PropTypes.string,
    title: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number,]).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number,]),
    incrementLiked: PropTypes.func,
    isSelected: PropTypes.bool,
    toggleModal: PropTypes.func.isRequired,    
    likedHandler: PropTypes.func,
}
Card.defaultProps = {
    img: "",
    color: "",
    incrementLiked: ()=>{},
    isSelected: false,    
    likedHandler: ()=>{},
}


export default Card