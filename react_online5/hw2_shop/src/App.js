import { PureComponent } from 'react';

import Header from './components/Header/Header'
import ListCards from './components/ListCards/ListCards'
import Modal from './components/Modal/Modal'

import styles from './App.module.scss';

class App extends PureComponent {
  state = {
    cards: [],  
    isOpenModal: false,
    likedCounter: 0,
    cartCounter:0,
    
  }

  async componentDidMount() {

    if(localStorage.getItem('likedCounter'))
    { if(localStorage.getItem('cartCounter')){
        const cartCounter = localStorage.getItem('cartCounter')
        this.setState({cartCounter: JSON.parse(cartCounter)})}

    const likedCounter = localStorage.getItem('likedCounter')
     const arrayCards = localStorage.getItem('cards')

     this.setState({likedCounter: JSON.parse(likedCounter)})
     this.setState({cards: JSON.parse(arrayCards)})
    } else{
    const data = await fetch("./products.json").then(responce => responce.json())
   
    this.setState({ cards: data })
    if(localStorage.getItem('cartCounter')){
      const cartCounter = localStorage.getItem('cartCounter')
      this.setState({cartCounter: JSON.parse(cartCounter)})}
    }
  }

  async componentDidUpdate(){
    if(this.state.likedCounter > 0){
      const arrayCards = localStorage.getItem('cards')
      this.setState({cards: JSON.parse(arrayCards)})
    }else {
      const data = await fetch(`./products.json`).then(res=>res.json())
    this.setState({cards: data})
    }
  }

  likedHandler = (id, isSelected) => {
    this.setState((prev)=>{
      const arrayCards = [...prev.cards]
       console.log(arrayCards)
       const index = arrayCards.findIndex(el => el.id === id)
       console.log(this.state.cards[index])
       arrayCards[index].isSelected = !isSelected
       console.log(this.state.cards[index])
       localStorage.setItem('cards', JSON.stringify(arrayCards))
       return arrayCards
   })
}

incrementLiked = (isSelected) => {
  if(isSelected){this.setState((prev)=>({
  
    likedCounter: prev.likedCounter + 1  }),
      () =>{
          localStorage.setItem('likedCounter', JSON.stringify(this.state.likedCounter))
      }
  )} else {this.setState((prev)=>({
  
    likedCounter: prev.likedCounter - 1  }),
      () =>{
          localStorage.setItem('likedCounter', JSON.stringify(this.state.likedCounter))
      }
  )}  
      }
  
      incrementCart = () => {
        this.setState((prev)=>({ cartCounter: prev.cartCounter + 1  }),
            ()=>{
                localStorage.setItem('cartCounter', JSON.stringify(this.state.cartCounter))
            })

    }



  toggleModal= (value) => {
    this.setState({isOpenModal: value})
}


  render() {
    const { cards, likedCounter, cartCounter, isOpenModal, } = this.state
    return (
      <div className={styles.App}>
        <Header likedCounter={likedCounter} cartCounter={cartCounter}/>
        <ListCards cards={cards} likedHandler={this.likedHandler} incrementLiked={this.incrementLiked} toggleModal={this.toggleModal} textButton ="add to cart"/>
        {isOpenModal &&  <Modal isOpenModal={isOpenModal} closeButton incrementCart={this.incrementCart} toggleModal={this.toggleModal}/>}
       
      </div>
    );
  }

}

export default App;
