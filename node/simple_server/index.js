const http = require('http');
const host = 'localhost';
const port = process.argv[2]?.slice(-4) || 3000;

let count = 0

const requestListener = ( req, res)=>{
     res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
    const message = JSON.stringify(
        {
            message:"Request handled successfully",
            requestCount: ++count
        }
    ) 
    res.end(message)
}

const server = http.createServer(requestListener)
server.listen(port, host, ()=>{
   
    console.log(`server on port: ${port}, host: ${host}`);
    
})