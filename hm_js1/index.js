// Відповіді на теоретичні питання

// 1. Змінну можна оголосити через let або const
// 2. Функція prompt приймає від користувача повідомлення в форматі string, а confirm - boolean.
// 3. Неявне приведення типів відбувається, коли ми не використовуємо для цього спеціальний метод , але дія виконується за замовчуванням. Неприклад: 32/"4" поверне 8. На етапі виконання   стрінгове значення '4' приводиться до типу Number.

// task 1

let name = "Tetiana";
let admin = name;
console.log(admin);

// task 2

let days = 7;
let secDay = days * 24 * 60 * 60;
console.log(`There are ${secDay}s in ${days} days`);

// task 3

console.log(prompt("How old are you?"));

