
const calcNumbers = (numberOne, numberTwo, operation) => {
    let result
    switch(operation){

        case "+":
            result = numberOne + numberTwo;
            break;

        case  "-":
            result = numberOne - numberTwo;
            break;
        
        case  "*":
            result = numberOne * numberTwo;
            break;

        case  "/":
            result = numberOne / numberTwo;
            break;
    }
    return result;

}

const getNumber = (message) => {
    while(isNaN(+message) || +message === 0){
        message = prompt("Enter correct number", message);
    }
    return +message;     
}
debugger
const getOperation = (message) => {
    while (message !== "+" && message !== "-" && message !== "/" && message !== "*"){
        message = prompt("Enter correct operation", message);
    }
    return message;
} 


console.log(calcNumbers(getNumber(prompt('Enter first number')), getNumber(prompt('Enter second number')), getOperation(prompt('Enter operation'))));

