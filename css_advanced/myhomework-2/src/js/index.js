
function handleBurger(){
    document.querySelector(".header__burger").addEventListener("click", ()=>{        
        document.querySelector(".header__list").classList.toggle("active")
        document.querySelector(".header__burger").classList.toggle("active")
    })
}
window.addEventListener("DOMContentLoaded", ()=>{
    handleBurger()
})