import React from "react";
import styles from "./Button.module.scss"

class Button extends React.PureComponent {
   
    render(){
        const {text, handleClick, backgroundColor} = this.props;
        return(
        <button type="button" style ={{ backgroundColor}}className={styles.btn} onClick={handleClick}>{text}</button>
      
  )
}
}

export default Button;