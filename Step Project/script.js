// section our services

let menuService = document.querySelector(".service-menu")
let serviceItem = document.querySelectorAll(".service-menu-item")
let serviceContent = document.querySelectorAll(".service-content")


menuService.addEventListener("click", function(e) {

    for(let i = 0; i < serviceContent.length; ++i){

        if(e.target.dataset.list == serviceContent[i].dataset.text){
            serviceContent[i].style.display = "flex"
        }
        else{
            serviceContent[i].style.display = "none"
        }
    }
})

menuService.addEventListener("click", function(e){

    for(let i=0;i<serviceItem.length;i++){

        serviceItem[i].classList.toggle("click-menu", false)
    }

    e.target.classList.toggle("click-menu")
})

// section amasing work

let menuWork = document.querySelector(".work-menu")
let workItem = document.querySelectorAll(".work-menu-item")
let workContent = document.querySelectorAll(".work-list")

menuWork.addEventListener("click", function(event){

    for (let i = 0; i < workContent.length; i++){

        if(event.target.dataset.tab == 'all' || event.target.dataset.tab == workContent[i].dataset.content){
            workContent[i].style.display = "block"
        }
        else{
            workContent[i].style.display = "none"
        }        
    }
    for(let i=0;i<workItem.length;i++){

        workItem[i].classList.toggle("work-menu-active", false)
    }

    event.target.classList.toggle("work-menu-active")
})

let loadButton = document.getElementById("load")
let hiddenContent = document.querySelectorAll(".hidden-list")

loadButton.addEventListener("click",function(e){

    for(let i = 0; i < hiddenContent.length; i++){
        hiddenContent[i].classList.add("work-list")
        hiddenContent[i].classList.remove("hidden-list")

        let currentItem = document.querySelector(".work-menu-active")
        if(hiddenContent[i].dataset.content === currentItem.dataset.tab || currentItem.dataset.tab === "all"){
            hiddenContent[i].style.display = "block"
        } 
        else hiddenContent[i].style.display = "none"
    } 

    loadButton.style.display = "none"
    workContent = document.querySelectorAll(".work-list")
})

// section client feedback

let rightSlider = document.querySelector(".right-slider")
let leftSlider = document.querySelector(".left-slider")
let clientSlider = document.querySelector(".client-slider")
let clientQuote = document.querySelectorAll(".client-item")
let sliderPhoto = document.querySelectorAll(".slider-item")

clientSlider.addEventListener("click", function(event){

    for (let i = 0; i < clientQuote.length; i++){
      
        if(event.target.closest.className = "photo-slider"){  

            if(event.target.parentElement.dataset.photo == clientQuote[i].dataset.quote){
                clientQuote[i].style.display = "block"                             
            }
            else{
                clientQuote[i].style.display = "none"
                                
            }    
        }
    }
    getClass()   
})


let currentSlide = 0
function getCurrentSlide(){    
    
    for(let i = 0; i <clientQuote.length; i++ ){
       if (clientQuote[i].style.display == "block"){
           currentSlide = i           
       }        
    }    
    return currentSlide
}

function getClass(){
    getCurrentSlide()
    for(let i = 0; i< sliderPhoto.length; i++){       
        if(i==currentSlide){
sliderPhoto[i].classList.add("client-transrom")
        }
        else{
            sliderPhoto[i].classList.remove("client-transrom") 
        }
    }
}


    leftSlider.addEventListener("click", function(e){
        let currentSlide = getCurrentSlide()  
        
        if(currentSlide > 0 && currentSlide < clientQuote.length){ 

             clientQuote[currentSlide].style.display = "none" 

             clientQuote[currentSlide - 1].style.display = "block"            
        }
        getClass()        
    })


    rightSlider.addEventListener("click", function(e){
        let currentSlide = getCurrentSlide() 
        
        if(currentSlide < clientQuote.length - 1 )  { 

            clientQuote[currentSlide].style.display = "none"            
            clientQuote[currentSlide +1].style.display = "block"           
        } 
        getClass()       
    })

