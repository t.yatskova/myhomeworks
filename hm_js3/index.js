let numberOne = +prompt ("Enter first number");
while(isNaN(numberOne) || numberOne === 0 || !Number.isInteger(numberOne)){
    numberOne = +prompt("Enter first number", numberOne)
}

let numberTwo = +prompt ("Enter second number");
while(isNaN(numberTwo) || numberTwo === 0 || !Number.isInteger(numberTwo)){
    numberTwo = +prompt("Enter second number", numberTwo)
}

let smallerNumber = 0;
let biggerNumber = 0;

if(numberOne > numberTwo){
    smallerNumber = numberTwo;
    biggerNumber = numberOne;

} else{
    smallerNumber = numberOne;
    biggerNumber = numberTwo;
}

// цикл для визначення кратності числа

let arrFive = [];

for  (let i = smallerNumber; i <= biggerNumber; i++ ){
    if(i % 5 === 0){
        arrFive.push(i);       
       }       
    }

if (arrFive.length === 0) {
    console.log('Sorry, no numbers');

 } else{
    console.log(`Числа, кратні 5: ${arrFive.join(",")}`);
 }

//  цикл для визначення простих чисел

let arrSimples = [];

for  (let i = smallerNumber; i <= biggerNumber; i++ ){
    for(let j = 2; j <= i; j++){ 
        let result = i / j;
        if(Number.isInteger(result))       
        arrSimples.push(i)
       
    }  
    if(arrSimples.length === 1){
        console.log(`Просте число: ${arrSimples.join(", ")}`);           
 }
 arrSimples.length = 0;   
}
