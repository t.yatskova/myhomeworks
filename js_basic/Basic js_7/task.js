//DOM - это браузерное представление HTML-документа в виде дерева, состоящего из узлов.
let arrString = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
let attMess = ["1", "2", "3", "sea", "user", 23]
let arrTown = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]

function createList(arr, parent = document.body){ 

    let list = arr.map(function(item){

        if( Array.isArray(item)){ 
        let itemInner = createList(item, item)
        return `<ul> ${itemInner}</ul>`        
        }

        else {return `<li> ${item}</li>`}        
    } )  

    return parent.innerHTML = list.join(``)
}

const parentList = document.createElement("ul")
document.body.prepend(parentList)

createList(arrTown, parentList)

function clearPage(){
  let clear =  document.body.innerHTML = ""
  return clear
}

setTimeout(clearPage, 3000)//к сожалению, без таймера
