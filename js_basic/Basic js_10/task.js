let check = document.querySelectorAll(".icon-password")

let messageError = document.createElement("p")
messageError.innerText = "Нужно ввести одинаковые значения"
messageError.style.color = "red" 

check.forEach(element => { 
    element.addEventListener("click", function
    (event) {
        let elInput = event.target.parentElement.children[0]

        if(elInput.getAttribute("type") === "password"){
            event.target.classList.toggle("fa-eye-slash", false)
            event.target.classList.toggle("fa-eye", true)
            elInput.setAttribute("type", "text")
    
        }
        
        else{
            event.target.classList.toggle("fa-eye-slash", true)
            event.target.classList.toggle("fa-eye", false)
            elInput.setAttribute("type", "password")
        }
    })
})

let submit = document.querySelector(".btn")
let inputSubmit = document.querySelector(".input-submit")
let inputPassword = document.querySelector(".input-password")

submit.addEventListener("click", function(event) {
    event.preventDefault() 

    messageError.remove()

    if(inputPassword.value === inputSubmit.value) {
        alert("You are welcom")
    }

    else{          
        inputSubmit.after(messageError)
    }
})
  