//1. Функция setTimeout вызываетзадержку выполнения кода на заданное время один раз, а setInterval повторяет код через заданное время
// 2.Из-за ограничения на частоту выполнения несмотря на нулевой setTimeout, задержка может соствлять до 4 секунд
// 3. для функции setInterval резервируется место в памяти на все время ее выполнения. Кроме нее там же хранятся все переменные, на которые она ссылается. Поэтому для экономии лучше отменять ее, как только в функции не будет необходимости.

let images = document.querySelectorAll(".image-to-show")
let timerId 
let btnShow = document.querySelector(".run")
let index = 0
let stop = document.querySelector(".stop")
console.log(btnShow);
getImage()
function getImage(){ 
    clearTimeout(timerId)
    if(index > 0) {
        images[index-1].style.display = "none"
    }
    
    if (index >= images.length){
        index = 0
    }
    images[index].style.display = "block"

    index++;
    btnShow.disabled = true;
    timerId = setTimeout(getImage,3000)

}

stop.addEventListener("click",() => {clearTimeout(timerId)
btnShow.disabled = false
})


btnShow.addEventListener("click",() =>getImage())
