//forEach перебирает элементы массива, применяет к каждому из них заданные условия, но не возвращает результат 

function filterBy(arr, type){
    let arrFilter = []
    for(key in arr){
        if (typeof(arr[key]) !== type){
            arrFilter.push(arr[key])
        }
    }  
    
    return arrFilter
}
let firstArray = ["hello", "world", "23",23, null]
let newArr = filterBy(firstArray, "string")
console.log(newArr)