window.onload = function() {
    if(localStorage.getItem("theme") === "light"){
        changeTheme()
    }
    if(localStorage.getItem("theme")==="dark"){
        createTheme()
    }
}

let span = document.querySelector(".header-title span")
let menu = document.querySelector(".header-menu")
let menuLink = document.querySelectorAll(".header-menu-link")
let asideLink = document.querySelectorAll(".aside-menu-item-link")
let footerLink = document.querySelectorAll(".footer-menu-link")
let footer = document.querySelector(".footer")

let btn = document.querySelector(".change")
let light = btn.style.backgroundColor = "turquoise"
let dark = btn.style.backgroundColor = "rgba(99, 105, 110, 0.48)"


function changeTheme(){
    span.style.color = "turquoise"
    menu.style.backgroundColor = "#35444F"    
    menuLink.forEach((elem) => elem.style.color = "#FFFFFF")
    asideLink.forEach((elem) => elem.style.color = "#495863")
    footerLink.forEach((elem) => elem.style.color = "#0C1927")
    footer.style.backgroundColor = "rgba(99, 105, 110, 0.48)"
    btn.style.backgroundColor =  "rgba(99, 105, 110, 0.48)"
    localStorage.setItem("theme", "light")
    
}
function createTheme(){
    span.style.color = "#1E6A50"
    menu.style.backgroundColor = "#77B49C"    
    menuLink.forEach((elem) => elem.style.color = "#CD1941")
    asideLink.forEach((elem) => elem.style.color = "#CD1941")
    footerLink.forEach((elem) => elem.style.color = "#CD1941")
    footer.style.backgroundColor = "#77B49C"
    btn.style.backgroundColor = "turquoise"
    localStorage.setItem("theme", "dark")
    
}

btn.addEventListener("click",() => {
    if(btn.style.backgroundColor === light){
        changeTheme()        
    }
    else{
        createTheme()        
    }
})