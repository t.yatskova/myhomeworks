//Обработчик событий - это функция, которая вызывается, когда это событие происходит 


const label = document.createElement('p')
document.body.append(label)
label.innerText = "Price"

const field = document.createElement("input")
label.append(field)

const text = document.createElement("span")

const btn = document.createElement("button")
btn.innerText = "X"

const alert = document.createElement("p")
alert.innerText = "Please enter correct price"


field.addEventListener("focus", function(){
    this.style.border = "1px solid green"
    field.value = "" 
    alert.style.display = "none"
    field.style.color = "black"
})

field.addEventListener("blur", function(){
    if(isNaN(field.value) || field.value < 0){
        field.style.border = "1px solid red"
        field.after(alert)
        alert.style.display = "block"
     }

    else {  this.style.border = "1px solid grey"   
    label.before(text) 
    text.innerText = `Текущая цена: ${field.value}`   
    text.after(btn)
     alert.style.display = "none"
    
    field.style.color = "green"}
})

btn.addEventListener("click", function(){
    this.remove()
    text.remove()
    field.value = ""
})




