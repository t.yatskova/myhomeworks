//1.Функция это часть кода, отвечающая за какое-то отдельное действие. Она позволяет тиражировать функционал, не увеличивая объем кода. Используя функцию легче вносить необходимые изменения, не нарушая общую структуру.
//2.Аргумент задает конкретное значение параметру функции. Сама фунция описывает универсальный способ выполнения действия. Аргумент позволяет вычислить конкретное значение

let numberFirst = +prompt("Enter first number")

while(isNaN(numberFirst) || numberFirst === 0)
{numberFirst = +prompt("Enter correct number", numberFirst)}

let numberSecond = +prompt("Enter second number")

while(isNaN(numberSecond) || numberSecond === 0){
    numberSecond = +prompt("Enter correct number", numberSecond)}

let operation = prompt("Enter your operation")

 while(operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/" ){
    operation = prompt("Enter correct operation")
}

function result(numberFirst, operation, numberSecond) {
    let resultOperation = 0
    switch(operation){
        case "+":
            resultOperation = numberFirst + numberSecond
            break
        case "-":
            resultOperation = numberFirst - numberSecond
            break 
        case "*" :
            resultOperation = numberFirst * numberSecond
            break 
        case "/" :
            resultOperation = numberFirst / numberSecond 
            break
    }
     return resultOperation
}

let res = result(numberFirst, operation, numberSecond)
console.log(result(numberFirst, operation, numberSecond))

