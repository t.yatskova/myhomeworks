
// Работа с input: пользователь может вводить данные с помощью голосового набора или копированием с помощью мыши. Поэтому событий клавиатуры для работы с полем ввода недостаточно.

let btn = document.querySelectorAll(".btn")

document.addEventListener("keyup", function(event){
    btn.forEach( (elem)=>{

      if(event.key.toLowerCase() == elem.textContent.toLowerCase()) {
        elem.classList.toggle('active')
      }

      else {
        elem.classList.remove('active');
      }
    })
})

