"use strict"

// https://ajax.test-danit.com/api/swapi/films

const body = document.querySelector("body")



const getPromise = (url) => {
  fetch(url).then(response => response.json()).then(filmArr => {

    filmArr.forEach(({ characters, name, episodeId, openingCrawl }) => {
      new Card(name, episodeId, openingCrawl).render(body)
      getMovieCharacters(characters).then(characterList => new List(characterList).render(episodeId))
    })
  })
}

function getMovieCharacters(links) {
  return new Promise((resolve, reject) => {
    const characters = [];
    links.forEach(link => {
      fetch(link)
        .then(response => response.json())
        .then(character => {
          characters.push(character.name)
          if (characters.length === links.length) {
            resolve(characters)
          }
        })
    })
  })
}

class Card {
  constructor(name, episodeId, openingCrawl) {
    this.name = name;
    this.openingCrawl = openingCrawl;
    this.episodeId = episodeId;
    this.container = document.createElement('ul')

    this.title = document.createElement('h3')
    this.subtitle = document.createElement('p')
    this.content = document.createElement('p')
    this.wrapper = document.createElement("div")


  }

  render(body) {
    this.title.innerText = ` ${this.name}`
    this.subtitle.innerText = ` ${this.episodeId}`
    this.content.innerText = `${this.openingCrawl}`

    this.wrapper.id = this.episodeId
    this.wrapper.append(this.title, this.subtitle, this.content)
    body.append(this.wrapper)
  }
}

class List {
  constructor(value) {

    this.value = value
    this.item = document.createElement('li')

  }
  render(id) {
    const container = document.getElementById(id)
    container.insertAdjacentHTML('beforeend', `<ul>${this.value.map(item => {
      return `<li>${item}</li>`
    }).join('')}</ul>`)
  }
}


getPromise('https://ajax.test-danit.com/api/swapi/films')



