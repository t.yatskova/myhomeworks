const wrapper = document.querySelector('.container')
const findButton = document.querySelector('.find__button')

const createEvent = () => {    
    findButton.addEventListener('click', () => {
        getIP()
        findButton.disabled = true
    })
}

const getIP = async () => {
   try{
    let ipResponse = await fetch("https://api.ipify.org/?format=json")
   let ipData = await ipResponse.json()
   let arealResponse = await fetch(`http://ip-api.com/json/${ipData.ip}`)
    let arealData = await arealResponse.json()
    const {timezone, regionName, country, city, countryCode} =arealData
   createUserData({timezone, regionName, country, city, countryCode})
   }
   catch (error){
    console.error(error);
   }
}   

createEvent()

const createUserData = (dataObject) => {
    const title = document.createElement('h3')
    title.innerText = "I catch you"
    findButton.after(title)
    const ul = document.createElement("ul")
    for (const key in dataObject){
      const li =  document.createElement("li")
        li.innerText = `${key} : ${dataObject[key]}` 
        ul.append(li)       
    }
    wrapper.append(ul)

}