
class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }
    get age() {
        return this._age
    }
    get salary() {
        return this._salary
    }

    set name(value){
        this._name = value;        
    }
    set age(value){
        this._age = value;  
    }
    set salary(value) {
        this._salary = value;  
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)        
        this.lang = lang;        
    }

    get salary(){
    
    return  super.salary * 3 
    }    
}

const prog1 = new Programmer("Andy", 19, 1000, ['eng', 'ukr'])
console.log(prog1);

const prog2 = new Programmer("Menny", 25, 1200, ['eng', 'jpn'])
console.log(prog2);

const prog3 = new Programmer("Matew", 29, 2000, ['eng', 'cheh', 'ukr'])
console.log(prog3);
console.log(prog3.salary);
