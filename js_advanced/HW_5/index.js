'use strict'


const container = document.querySelector('.container')

const getData = async () => {
    try{
    const fetch1 = await fetch('https://ajax.test-danit.com/api/json/users')
        .then(response => response.json())
        .then(dataUser => dataUser)
    console.log(fetch1);
    const fetch2 = await fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(dataPosts => dataPosts)
       
    fetch2.forEach(({id: postId, title, body, userId} )=>{
       const user = fetch1.find(user => user.id === userId);
       new UserCard(user.id, user.name, user.email, user.username, postId, title, body, userId).render(container)
    }) 
    }
    catch(err){
        console.error(err)
    }   
    }

getData()
    
class UserCard {
    constructor(id, name, email, username, postId, title, body, userId) {
        this.postId = postId
        this.userId = userId;
        this.name = name;       
        this.id = id;
        this.username = username;
        this.email = email;
        this.body = body;
         this.title = title;        
        this.card = document.createElement('div')
        this.deleteButton = document.createElement('button')
        

        this.deleteButton.addEventListener('click', () =>{        
                          
              fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json'},
                body: JSON.stringify(),
            })
             .then(response => response.json)
             .then(result => 
                this.card.remove())
            });
    }
  

    createElements() {       
            this.deleteButton.innerHTML = 'X'; 
            this.deleteButton.setAttribute("id", `${this.postId}`)
            this.deleteButton.classList.add('del-button')               
            this.card.insertAdjacentHTML('beforeend',
                `<div class = "user-nickname">${this.username}</div>
                <p class = "user-data">${this.name} <a class = "user-link" href ="#">${this.email}</a></p> 
                <h4 class = "post-title">${this.title}</h4>
                <p class = "post-body">${this.body}</p>`)
            this.card.classList.add("card")    
            this.card.insertAdjacentElement('afterbegin', this.deleteButton)        

    }

    render(container) {
        this.createElements()   
        container.append(this.card)
    }
}