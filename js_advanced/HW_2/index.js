const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

class DataError extends Error {
  constructor(value) {
    super();
    this.message = `${value} is not a valid`;   
    this.name = 'DataError';    
  }
}

class BookList { 
  constructor(key, value) {
    this.key = key;
    this.value = value;
    this.item = document.createElement('li')
    this.listWrapper = document.createElement('ul')
  }

  createElements(){
    this.item.innerText = `${this.key} : ${this.value}`
    this.listWrapper.append(this.item)
  }

  render(container = document.body){
    this.createElements()
    container.append(this.listWrapper);
  }
}

books.forEach((book) => {

  try {
    if (book.author === undefined) {
      throw new DataError(`"author"`)

    } else if (book.name === undefined) {
      throw new DataError(`"name"`)

    } else if (book.price === undefined) {
      throw new DataError(`"price"`)

    }

    for (key in book) {
      new BookList(key, book[key]).render(document.querySelector('#root'))
    }
  }

  catch (err) {
    if (err.name === 'DataError') {
      console.log(err);
      
    } else {
      throw err
    }
  }
})





